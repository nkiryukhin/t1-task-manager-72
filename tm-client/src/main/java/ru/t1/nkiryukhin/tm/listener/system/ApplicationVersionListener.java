package ru.t1.nkiryukhin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.dto.request.ApplicationVersionRequest;
import ru.t1.nkiryukhin.tm.dto.response.ApplicationVersionResponse;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show application version";
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

}
