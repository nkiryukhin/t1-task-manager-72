package ru.t1.nkiryukhin.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.nkiryukhin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@Table(name = "web_project")
@NoArgsConstructor
@AllArgsConstructor
public final class Project {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    private String name = "";

    @Column
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Column
    @NotNull
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @ManyToOne
    private User user;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return id + " : " + name + " : " + description + " : " + status.getDisplayName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(getId(), project.getId())
                && Objects.equals(created, project.created)
                && Objects.equals(description, project.description)
                && name.equals(project.name)
                && status == project.status;
    }

}