package ru.t1.nkiryukhin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.nio.file.AccessDeniedException;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface TaskEndpoint {

    @WebMethod
    @DeleteMapping("/clear")
    void clear() throws AccessDeniedException;

    @WebMethod
    @GetMapping("/count")
    long count() throws AccessDeniedException;

    @WebMethod
    @DeleteMapping("/deleteList")
    void deleteList(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<TaskDTO> tasks) throws AccessDeniedException;

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task) throws AccessDeniedException;

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id) throws AccessDeniedException;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) throws AccessDeniedException;

    @WebMethod
    @GetMapping("/findAll")
    Collection<TaskDTO> findAll() throws AccessDeniedException;

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable String id
    ) throws AccessDeniedException;

    @WebMethod
    @PutMapping("/save")
    void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    ) throws AccessDeniedException;

}