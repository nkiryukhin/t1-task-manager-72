package ru.t1.nkiryukhin.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.nkiryukhin.tm.dto.TaskDTO;
import ru.t1.nkiryukhin.tm.dto.UserDTO;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;
import ru.t1.nkiryukhin.tm.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public final class TaskRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");

    @NotNull
    private static final HttpHeaders HEADER = new HttpHeaders();

    @Nullable
    private static String USER_ID;

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        HEADER.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        @Nullable String sessionId = cookies.stream().filter(item -> "JSESSIONID".equals(item.getName())).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        @NotNull final String urlProfile = "http://localhost:8080/api/auth/profile";
        @NotNull final ResponseEntity<UserDTO> responseProfile = restTemplate.exchange(urlProfile, HttpMethod.GET, new HttpEntity<>(HEADER), UserDTO.class);
        USER_ID = responseProfile.getBody().getId();
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<TaskDTO> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<TaskDTO> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, TaskDTO.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        restTemplate.postForEntity(logoutUrl, new HttpEntity<>(HEADER), Result.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "save";
        task1.setUserId(USER_ID);
        task2.setUserId(USER_ID);
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(task1, HEADER));
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(task2, HEADER));
    }

    @After
    public void finishTest() {
        @NotNull final String url = BASE_URL + "clear";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(HEADER));
    }

    @Test
    public void clear() {
        @NotNull String url = BASE_URL + "clear";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(HEADER));
        url = BASE_URL + "findAll";
        Assert.assertEquals(0, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void countTasksTest() {
        @NotNull final String url = BASE_URL + "count";
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        Assert.assertEquals(2, restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(HEADER), Long.class).getBody().longValue());
    }

    @Test
    public void deleteTest() {
        @NotNull String url = BASE_URL + "delete";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(task1, HEADER));
        url = BASE_URL + "findById/" + task1.getId();
        Assert.assertNull(sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
    }

    @Test
    public void deleteListTest() {
        @NotNull String url = BASE_URL + "deleteList";
        final List<TaskDTO> tasks = Arrays.asList(task1, task2);
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(tasks, HEADER));
        url = BASE_URL + "findAll";
        Assert.assertEquals(0, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = BASE_URL + "findAll";
        Assert.assertEquals(2, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull String url = BASE_URL + "findById/" + task1.getId();
        Assert.assertEquals(task1.toString(), sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().toString());
        url = BASE_URL + "findById/" + task2.getId();
        Assert.assertEquals(task2, sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
    }

    @Test
    public void saveTest() {
        @NotNull String url = BASE_URL + "save";
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(task3, HEADER));
        url = BASE_URL + "findById/" + task3.getId();
        Assert.assertEquals(task3, sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
    }

}