package ru.t1.nkiryukhin.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.nkiryukhin.tm.dto.UserDTO;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;
import ru.t1.nkiryukhin.tm.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public final class UserRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/users/";

    @NotNull
    private final UserDTO user1 = new UserDTO("Test User 1");

    @NotNull
    private final UserDTO user2 = new UserDTO("Test User 2");

    @NotNull
    private static final HttpHeaders HEADER = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=admin&password=admin";
        HEADER.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        @Nullable String sessionId = cookies.stream().filter(item -> "JSESSIONID".equals(item.getName())).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        @NotNull final String urlProfile = "http://localhost:8080/api/auth/profile";
        @NotNull final ResponseEntity<UserDTO> responseProfile = restTemplate.exchange(urlProfile, HttpMethod.GET, new HttpEntity<>(HEADER), UserDTO.class);
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<UserDTO> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<UserDTO> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, UserDTO.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        restTemplate.postForEntity(logoutUrl, new HttpEntity<>(HEADER), Result.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "save";
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(user1, HEADER));
    }

    @After
    public void finishTest() {
        @NotNull final String url = BASE_URL + "deleteById/" + user1.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER));
    }

    @Test
    public void countUsersTest() {
        @NotNull final String url = BASE_URL + "count";
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        Assert.assertEquals(4, restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(HEADER), Long.class).getBody().longValue());
    }

    @Test
    public void deleteTest() {
        @NotNull String url = BASE_URL + "save";
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(user2, HEADER));
        url = BASE_URL + "delete";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(user2, HEADER));
        url = BASE_URL + "findById/" + user2.getId();
        Assert.assertNull(sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = BASE_URL + "findAll";
        Assert.assertEquals(4, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull String url = BASE_URL + "findById/" + user1.getId();
        Assert.assertEquals(user1.getLogin(), sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getLogin());
    }

    @Test
    public void saveAndDeleteByIdTest() {
        @NotNull String url = BASE_URL + "save";
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(user2, HEADER));
        url = BASE_URL + "findById/" + user2.getId();
        Assert.assertEquals(user2.getLogin(), sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getLogin());
        url = BASE_URL + "deleteById/" + user2.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER));
    }

}