package ru.t1.nkiryukhin.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nkiryukhin.tm.api.service.model.IProjectService;
import ru.t1.nkiryukhin.tm.api.service.model.IProjectTaskService;
import ru.t1.nkiryukhin.tm.api.service.model.ITaskService;
import ru.t1.nkiryukhin.tm.api.service.model.IUserService;
import ru.t1.nkiryukhin.tm.configuration.ServerConfiguration;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.TaskIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.migration.AbstractSchemeTest;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.model.Task;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.*;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private static final ITaskService taskService = context.getBean(ITaskService.class);

    @NotNull
    private static final IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private static final IProjectTaskService projectTaskService = context.getBean(IProjectTaskService.class);

    @NotNull
    private static final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private static final Liquibase liquibase = liquibase("changelog/changelog-master.xml");

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void loadTestData() {
        userService.add(USUAL_USER);
        projectService.add(USUAL_PROJECT1);
        projectService.add(USUAL_PROJECT2);
        taskService.add(USUAL_TASK1);
        taskService.add(USUAL_TASK2);
    }

    @After
    public void removeTestData() throws AbstractException {
        for (Task task : TASK_LIST) {
            taskService.removeById(task.getUser().getId(), task.getId());
        }
        for (Project project : PROJECT_LIST) {
            projectService.removeById(project.getUser().getId(), project.getId());
        }
        userService.removeById(USUAL_USER.getId());
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(null, USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject("", USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), "", USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT2.getId(), USUAL_TASK1.getId());
        Task task = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(USUAL_PROJECT2, task.getProject());
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.removeProjectById(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.removeProjectById("", USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.removeProjectById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.removeProjectById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            projectTaskService.removeProjectById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID);
        });
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK2.getId());
        projectTaskService.removeProjectById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNull(projectService.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
        Assert.assertNull(taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId()));
        Assert.assertNull(taskService.findOneById(USUAL_USER.getId(), USUAL_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(null, USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject("", USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), "", USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        projectTaskService.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        Task task = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertNull(task.getProject());
        projectTaskService.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
    }

}

